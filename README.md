This repository contains the  source code for the software used in the experiment  "The effects of acute work stress and appraisal on psychobiological stress responses in a group office environment". 

For questions, please contact the maintainer of the repository or the authors of the paper