﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ExperimentPrototype
{
    class EmailImport
    {
        public EmailImport()
        {

        }

        public Queue<SimulatedEmail> importAllEmails(string path)
        {
            Queue<SimulatedEmail> mails = new Queue<SimulatedEmail>();

            string[] filepaths = Directory.GetFiles(path + @"Emails\", "email*");

            foreach (string filepath in filepaths)
            {
                mails.Enqueue(getEmailFromText(filepath));
            }

            List<SimulatedEmail> sortedMail = mails.ToList<SimulatedEmail>().OrderBy(o => o.blockNumber).ThenBy(c => c.blockOrder).ToList();

            return new Queue<SimulatedEmail>(sortedMail); ;
        }

        public SimulatedEmail getEmailFromText(string path)
        {
            SimulatedEmail an_email = new SimulatedEmail();

            System.Xml.Serialization.XmlSerializer reader = new System.Xml.Serialization.XmlSerializer(typeof(SimulatedEmail));
            System.IO.StreamReader file = new System.IO.StreamReader(
                path);
            an_email = (SimulatedEmail)reader.Deserialize(file);
            file.Close();

            an_email.fileName = path;
            return an_email;
        }
    }
}
