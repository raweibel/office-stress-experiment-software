﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.IO;
using System.Windows.Controls;
using System.Windows.Xps.Packaging;
using System.Windows.Media;

namespace ExperimentPrototype
{
    public partial class MainWindow
    {
        DateTime lastEmail;

        private void EmailSelectedInOverview(object sender, SelectionChangedEventArgs e)
        {
            SimulatedEmail selectedItem = null;
            ListBox ItemsHost;

            var buff = sender as ListBox;

            ItemsHost = buff;

            if (e.AddedItems.Count > 0)
                selectedItem = (SimulatedEmail)e.AddedItems[0];
            if (selectedItem != null)
            {
                Sender.Text = selectedItem.from;
                Subject.Text = selectedItem.subject;
                Message.Text = selectedItem.mainText;

                selectedItem.read = true;
                if (selectedItem.attachmentFile != null && selectedItem.attachmentFile.Length > 0)
                {
                    Attachment1.Content = selectedItem.attachmentFile;
                    Attachment1.Visibility = Visibility.Visible;
                }
                else
                {
                    Attachment1.Visibility = Visibility.Hidden;
                }
                selectedEmail = selectedItem;

                EmailMessageBoxToTop();

                if (logVerbosityLevel >= 2)
                    logger.LogMessage("Opened email " + selectedItem.fileName, 4);
            }
            EmailOverview.Items.Refresh();
            ReadWindow.Visibility = Visibility.Visible;
            ReadWindowStart.Visibility = Visibility.Hidden;
        }

        private void EmailMessageBoxToTop()
        {
            if (VisualTreeHelper.GetChildrenCount(ReadWindow) > 0)
            {               
                ScrollViewer scrollViewer = (ScrollViewer)VisualTreeHelper.GetChild(ReadWindow, 8);
                scrollViewer.ScrollToTop();
            }
        }

        private void Attachment1_Click(object sender, RoutedEventArgs e)
        {

            if (selectedEmail != null)
            {
                string fileName = resourcesPath + @"Attachments\" + selectedEmail.attachmentFile;
                XpsDocument doc = new XpsDocument(fileName, FileAccess.Read);
                DocumentViewer.Document = doc.GetFixedDocumentSequence();
                Dispatcher.BeginInvoke((Action)(() => tabControl.SelectedItem = DocumentDisplay));
                if (logVerbosityLevel >= 2)
                    logger.LogMessage("Opened attachment " + selectedEmail.attachmentFile, 5);
            }

        }

        private void StartEmailTimer()
        {                        
            // Remove emails from blocks that are already done
            mails = new Queue<SimulatedEmail>(mails.Where(m => m.blockNumber > currentBlock).ToList());

            // Check for new email instantly
            lastEmail = DateTime.Now;
            if (currentBlock > 0)
                EmailSender_tick(null, null);

            emailTimer = new System.Windows.Threading.DispatcherTimer();
            emailTimer.Tick += new EventHandler(EmailSender_tick);
            emailTimer.Interval = emailInterval;
            emailTimer.Start();
        }

        private void EmailSender_tick(object sender, EventArgs e)
        {            
            if (mails.Count > 0)
            {
                //check number of unread emails
                int unreadMails = displayedMails.Where(p => !p.read & !p.subject.Contains("INSTRUKTION")).Count();

                //if less than three add one new email
                if (unreadMails < 3 || DateTime.Now.Subtract(lastEmail) > maxWaitEmail)
                {
                    lastEmail = DateTime.Now;
                    SimulatedEmail nextMail = mails.Peek();

                    if (nextMail.blockNumber - 1 == currentBlock)
                    {
                        nextMail = mails.Dequeue();
                        nextMail.received = DateTime.Now;
                        displayedMails.Insert(0, nextMail);
                        currentBlockOrder = nextMail.blockOrder;
                        EmailOverview.DataContext = this;
                        if (logVerbosityLevel >= 2)
                            logger.LogMessage("Received email " + nextMail.fileName, 3);
                    }
                }
            }
        }

        private void StartShortEmailTimer()
        {
            // Remove emails from blocks that are already done
            mails = new Queue<SimulatedEmail>(mails.Where(m => m.blockNumber > currentBlock).ToList());

            mailCounter = 0;

            specialMailTimer = new System.Windows.Threading.DispatcherTimer();
            specialMailTimer.Tick += new EventHandler(specialMailTick);
            specialMailTimer.Interval = new TimeSpan(0,0,2);
            specialMailTimer.Start();
        }

        private int mailCounter;

        private void specialMailTick(object sender, EventArgs e)
        {
            EmailSender_tick(null, null);
            specialMailTimer.Interval = new TimeSpan(0, 0, 58);
            mailCounter++;
            if (mailCounter > 2)
                specialMailTimer.Stop();
        }

    }
}
