﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExperimentPrototype
{
    public class SimulatedEmail
    {
        public string from { get; set; }

        public string subject { get; set; }

        public DateTime received { get; set; }

        public string mainText { get; set; }

        public bool read { get; set; }

        public string attachmentFile { get; set; }

        public int blockOrder { get; set; }

        public int blockNumber { get; set; }

        public string taskType { get; set; }

        public string fileName { get; set; }
    }
}
