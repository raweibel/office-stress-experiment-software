﻿using System;
using System.Windows.Data;
using System.Windows.Media;
using System.Globalization;

namespace ExperimentPrototype
{
    class MailStatusToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo cultureInfo)
        {
            SolidColorBrush brush = new SolidColorBrush();
            if ((bool)value)
            {
                brush.Color = Colors.Green;
            }
            else
            {
                brush.Color = Colors.White;
            }
            return brush;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo cultureInfo)
        {
            return new NotImplementedException();
        }
    }
}
