namespace ExperimentPrototype.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCategoryToLogEvent : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LogEvents", "Category", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.LogEvents", "Category");
        }
    }
}
