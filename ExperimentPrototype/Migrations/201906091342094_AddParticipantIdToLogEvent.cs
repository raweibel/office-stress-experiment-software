namespace ExperimentPrototype.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddParticipantIdToLogEvent : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LogEvents", "ParticipantId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.LogEvents", "ParticipantId");
        }
    }
}
