namespace ExperimentPrototype.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LogEvents",
                c => new
                    {
                        LogEventId = c.Int(nullable: false, identity: true),
                        LogMessage = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.LogEventId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.LogEvents");
        }
    }
}
