﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExperimentPrototype
{
    public class Instruction
    {
        public string text { get; set; }
        
        // Starts with 0
        public int block_id { get; set; }

        //0: Instruction
        //1: Questionnaire
        public int type { get; set; }

        public float duration { get; set; }
}
}
