﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ExperimentPrototype
{
    class InstructionImporter
    {
        public Queue<Instruction> importAllInstructions(string path)
        {
            Queue<Instruction> instructions = new Queue<Instruction>();            

            string[] filepaths = Directory.GetFiles(path+ @"Instructions\", "instruction*");
            
            foreach (string filepath in filepaths)
            {
                instructions.Enqueue(getInstructionFromFile(filepath));
            }

            return instructions;
        }

        public Instruction getInstructionFromFile(string filepath)
        {
            Instruction an_instruction = new Instruction();

            System.Xml.Serialization.XmlSerializer reader = new System.Xml.Serialization.XmlSerializer(typeof(Instruction));
            System.IO.StreamReader file = new System.IO.StreamReader(filepath);
            an_instruction = (Instruction)reader.Deserialize(file);
            file.Close();

            return an_instruction;
        }
    }
}
