﻿using System;
using System.Windows;

namespace ExperimentPrototype
{
    public partial class MainWindow
    {

        private void Instruction_Next_Button_Click(object sender, RoutedEventArgs e)
        {

            if (instructions.Count > 0)
            {
                loadNextInstruction();
            }
            else
            {
                InstructionScreen.Visibility = Visibility.Hidden;
                EndScreen.Visibility = Visibility.Visible;
            }
        }

        private void loadNextInstruction()
        {
            Instruction nextInstruction = instructions.Peek();
            if (nextInstruction.block_id == currentBlock)
            {
                instructions.Dequeue();
                switch(nextInstruction.type)
                {
                    case 0:
                        Instruction_Text.Text = nextInstruction.text;
                        if (nextInstruction.duration > 0)
                        {
                            Instruction_Next_Button.Visibility = Visibility.Hidden;
                            startInstructionSwitcher((int)(nextInstruction.duration * 60));
                        }
                        else
                        {
                            Instruction_Next_Button.Visibility = Visibility.Visible;
                        }
                        break;
                    case 1:
                        QuestionnairesBrowser.Navigate(nextInstruction.text + "?ParticipantID=" + participantId + "&BlockID=" + currentBlock);
                        Questionnaire.Visibility = Visibility.Visible;
                        if (nextInstruction.duration > 0)
                        {
                            QuestionnaireTimerDisplay.Visibility = Visibility.Visible;
                            startQuestionnaireClock(nextInstruction.duration);
                            startQuestionnaireChecker();
                        }
                        else
                        {
                            QuestionnaireTimerDisplay.Visibility = Visibility.Hidden;
                            startQuestionnaireChecker();
                        }
                        break;
                    case 2:
                        Instruction_Text.Text = nextInstruction.text;
                        Instruction_Next_Button.Visibility = Visibility.Hidden;
                        // Case 2 always has to be after a questionnaire!
                        TimeSpan diff = QuestionnaireStart.Subtract(DateTime.Now);
                        startInstructionSwitcher((int)diff.TotalSeconds);
                        break;
                }
            }
            else
            {
                InstructionScreen.Visibility = Visibility.Hidden;
                SynchronizationScreen.Visibility = Visibility.Visible;
                StartsyncDispatchTimer();
            }
        }

        private void startInstructionSwitcher(int seconds)
        {
            instructionSwitchTimer = new System.Windows.Threading.DispatcherTimer();
            instructionSwitchTimer.Tick += new EventHandler(switchInstruction);
            if (seconds > 0)
            {
                instructionSwitchTimer.Interval = new TimeSpan(0, 0, 0, seconds);
            } else
            {
                instructionSwitchTimer.Interval = new TimeSpan(0, 0, 0, 0);
            }
            instructionSwitchTimer.Start();
        }

        private void switchInstruction(object sender, EventArgs e)
        {
            instructionSwitchTimer.Stop();
            Instruction_Next_Button_Click(null, null);
        }

        private void startQuestionnaireChecker()
        {
            startQuestionnaireTimer = new System.Windows.Threading.DispatcherTimer();
            startQuestionnaireTimer.Tick += new EventHandler(checkQuestionnaireEnd);
            startQuestionnaireTimer.Interval = new TimeSpan(0, 0, 0, 2);
            startQuestionnaireTimer.Start();
        }

        private void startQuestionnaireClock(double minutes)
        {
            questionnaireTimer = new System.Windows.Threading.DispatcherTimer();
            questionnaireTimer.Tick += new EventHandler(setQuestionnaireClockTick);
            questionnaireTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
            questionnaireTimer.Start();
            QuestionnaireStart = DateTime.Now.AddMinutes(minutes);
        }

        private void setQuestionnaireClockTick(object sender, EventArgs e)
        {
            
            TimeSpan diff = QuestionnaireStart.Subtract(DateTime.Now);
            if (diff.TotalSeconds > 0)
            {
                QuestionnaireTimerDisplay.Content = diff.ToString(@"mm\:ss");
            }
            else
            {
                QuestionnaireTimerDisplay.Content = "00:00";
            }            
        }

        private void checkQuestionnaireEnd(object sender, EventArgs e)
        {
            startQuestionnaireTimer.Interval = new TimeSpan(0, 0, 0, 0,100);
            var document = (QuestionnairesBrowser.Document as mshtml.HTMLDocument);
            if (document != null)
            {
                var endOfSurvey = (QuestionnairesBrowser.Document as mshtml.HTMLDocument).getElementById("EndOfSurvey");
                if (endOfSurvey != null)
                {
                    Questionnaire.Visibility = Visibility.Hidden;
                    startQuestionnaireTimer.Stop();
                    if (questionnaireTimer != null)
                        questionnaireTimer.Stop();
                    QuestionnairesBrowser.Navigate("about:blank");

                    if (syncCounter == -1)
                    {
                        StartsyncDispatchTimer();
                        InstructionScreen.Visibility = Visibility.Hidden;
                        SynchronizationScreen.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        Instruction_Next_Button_Click(null, null);
                    }
                }
            }
        }

    }
}
