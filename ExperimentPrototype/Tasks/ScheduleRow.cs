﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExperimentPrototype
{
    public class ScheduleRow
    {
        public ScheduleRow() {
            Montag_morgen = new ScheduleEntry("",true);
            Dienstag_morgen = new ScheduleEntry("", true);
            Mittwoch_morgen = new ScheduleEntry("", true);
            Donnerstag_morgen = new ScheduleEntry("", true);
            Freitag_morgen = new ScheduleEntry("", true);
            Montag_nachmittag = new ScheduleEntry("", true);
            Dienstag_nachmittag = new ScheduleEntry("", true);
            Mittwoch_nachmittag = new ScheduleEntry("", true);
            Donnerstag_nachmittag = new ScheduleEntry("", true);
            Freitag_nachmittag = new ScheduleEntry("", true);
        }
        
        public string Mitarbeiter { get; set; }

        public ScheduleEntry Montag_morgen { get; set; }
        public ScheduleEntry Dienstag_morgen { get; set; }
        public ScheduleEntry Mittwoch_morgen { get; set; }
        public ScheduleEntry Donnerstag_morgen { get; set; }
        public ScheduleEntry Freitag_morgen { get; set; }

        public ScheduleEntry Montag_nachmittag { get; set; }
        public ScheduleEntry Dienstag_nachmittag { get; set; }
        public ScheduleEntry Mittwoch_nachmittag { get; set; }
        public ScheduleEntry Donnerstag_nachmittag { get; set; }
        public ScheduleEntry Freitag_nachmittag { get; set; }
    }
}
