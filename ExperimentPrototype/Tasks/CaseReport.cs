﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExperimentPrototype
{
    public class CaseReport
    {
        public int CaseReportId { get; set; }
        public string caseNumber { get; set; }
        public string clientNumber { get; set; }
        public string description { get; set; }
        public string cost_value { get; set; }
        public DateTime date { get; set; }
        public DateTime entryCreated { get; set; }
        public string ParticiapantId { get; set; }
    }
}
