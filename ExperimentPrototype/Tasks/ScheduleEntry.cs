﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExperimentPrototype
{
    public class ScheduleEntry
    {
        public ScheduleEntry(string an_entry, bool is_enabled)
        {
            Entry = an_entry;
            Enabled = is_enabled;
        }

        public string Entry{ get; set; }
        public bool Enabled{ get; set; }
    }
}
