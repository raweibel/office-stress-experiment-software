﻿
using System.Collections.Generic;

namespace ExperimentPrototype
{
    class SalesNumberNamesImporter
    {
        public SalesNumber[] importAgentNames(string path)
        {
            List<SalesNumber> names_list = new List<SalesNumber>();

            string line;

            // Read the file and display it line by line. 

            System.IO.StreamReader file = new System.IO.StreamReader( path + @"NumericNames.txt", System.Text.Encoding.Default, false);
            while ((line = file.ReadLine()) != null)
            {
                SalesNumber item = new SalesNumber();
                item.AgentName = line;                
                names_list.Add(item);
            }

            file.Close();

            return names_list.ToArray();
        }
    }
}
