﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExperimentPrototype
{
    class ClientListItem
    {
        public string lastName { set; get; }
        public string firstName { set; get; }
        public string clientNumber { set; get; }
        public string damagesEmployee { set; get; }
        public string insuranceEmployee { set; get; }
    }
}
