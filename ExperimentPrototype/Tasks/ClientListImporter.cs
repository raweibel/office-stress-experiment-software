﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExperimentPrototype
{
    class ClientListImporter
    {
        public ClientListItem[] importClientList(string path)
        {
            List<ClientListItem> a_clientList = new List<ClientListItem>();
            
            string line;

            // Read the file and display it line by line. 
            
            System.IO.StreamReader file = new System.IO.StreamReader(path + @"ClientList.txt", System.Text.Encoding.Default,false);
            while ((line = file.ReadLine()) != null)
            {
                ClientListItem item = new ClientListItem();
                string[] splitLine = line.Split('\t');
                item.lastName = splitLine[0];
                item.firstName = splitLine[1];
                item.clientNumber = splitLine[2];
                item.damagesEmployee = splitLine[3];
                item.insuranceEmployee = splitLine[4];
                a_clientList.Add(item);       
            }

            file.Close();

            return a_clientList.ToArray();
        }
    }
}
