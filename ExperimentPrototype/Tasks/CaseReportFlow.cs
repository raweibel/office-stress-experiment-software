﻿using ExperimentPrototype.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace ExperimentPrototype
{
    public partial class MainWindow
    {

        private void InsertCaseButton_Click(object sender, RoutedEventArgs e)
        {
            CaseReport a_case = new CaseReport();
            a_case.caseNumber = Casenumber.Text;
            a_case.clientNumber = Clientnumber.Text;
            a_case.description = Damage.Text;
            a_case.cost_value = Cost_Value.Text;

            if (CaseDate.SelectedDate != null)
                a_case.date = CaseDate.SelectedDate.Value;
            a_case.entryCreated = System.DateTime.Now;
            a_case.ParticiapantId = participantId;

            if (a_case.date != null & a_case.caseNumber != null & a_case.clientNumber != null & a_case.description != null & a_case.cost_value != null)
            {
                Casenumber.Text = "";
                Clientnumber.Text = "";
                Damage.Text = "";
                Cost_Value.Text = "";
                CaseDate.Text = "";
                if (logVerbosityLevel >= 3)
                {
                    using (var context = new CaseReportContext())
                    {
                        context.Database.Connection.Open();
                        context.CaseReports.Add(a_case);
                        context.SaveChanges();
                    }
                }                
            }
            DocumentViewer.Document = null;
            InsertCaseButton.IsEnabled = false;
        }

        private void Form_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (Damage.Text.Length > 0 && Casenumber.Text.Length > 0 && Clientnumber.Text.Length > 0 && Cost_Value.Text.Length > 0 && CaseDate.Text.Length > 0)
                InsertCaseButton.IsEnabled = true;
        }

        private void DatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Damage.Text.Length > 0 && Casenumber.Text.Length > 0 && Clientnumber.Text.Length > 0 && Cost_Value.Text.Length > 0 && CaseDate.Text.Length > 0)
                InsertCaseButton.IsEnabled = true;
        }
    }
}
