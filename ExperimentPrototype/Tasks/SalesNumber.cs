﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExperimentPrototype
{
    public class SalesNumber
    {
        public string AgentName { get; set; }

        public string Privathaftpflicht { get; set; }
        public string Hausrat { get; set; }
        public string Motorfahrzeug { get; set; }
        public string Lebensversicherung { get; set; }
        public string Total { get; set; }

        public int SalesNumberId { get; set; }
        public string participantId { get; set; }
        public int blockId { get; set; }
    }
}
