﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace ExperimentPrototype
{
    public partial class MainWindow
    {
        decimal num1 = 0;
        decimal num2 = 0;
        string Operator = "";
        bool opLast, resetLast = true, eqLast;

        private void input(string a)
        { 
            if (opLast || (txt_Main.Text == "0" && a != ".") || eqLast)
            {
                txt_Main.Text = a;
                eqLast = false;
            }
            else
                txt_Main.Text += a;
        }

        private void btn_O_Click(object sender, RoutedEventArgs e)
        {
            Button btn_Operator = (Button)sender;

            getNumbersAndCompute();

            Operator = btn_Operator.Content.ToString();           
            opLast = true;
            resetLast = false;
            eqLast = false;
        }

        private void btn_Number_Click(object sender, RoutedEventArgs e)
        {
            Button btn_Number = (Button)sender;
            input(btn_Number.Content.ToString());
            opLast = false;
        }

        private void btn_Dot_Click(object sender, RoutedEventArgs e)
        {
            opLast = false;
            if (txt_Main.Text != "")
            {
                if (!txt_Main.Text.Contains("."))
                    input(".");
            }
        }

        private void txt_Main_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void btn_Remove_Click(object sender, RoutedEventArgs e)
        {
            if (txt_Main.Text != "0")
            {
                if (txt_Main.Text.Length == 1)
                {
                    txt_Main.Text = "0";
                }
                else if (txt_Main.Text.Length > 0)
                {
                    txt_Main.Text = txt_Main.Text.Substring(0, txt_Main.Text.Length - 1);
                }
            }
        }

        private void btn_Clear_Click(object sender, RoutedEventArgs e)
        {
            txt_Main.Text = "0";
            Operator = "";
            num1 = 0;
            num2 = 0;
            opLast = false;
            resetLast = true;
            eqLast = false;
        }

        private void btn_Equal_Click(object sender, RoutedEventArgs e)
        {
            if (!resetLast)
            {
                getNumbersAndCompute();
                resetLast = true;
                num2 = 0;
                num1 = 0;
            }
           
            opLast = false;
            eqLast = true;
        }

        private void getNumbersAndCompute()
        {
            if (resetLast)
            {
                num1 = decimal.Parse(txt_Main.Text);
                resetLast = false;
            } else
            {
                num2 = decimal.Parse(txt_Main.Text);
                num1 = computeResult(Operator, num1, num2);
                txt_Main.Text = num1.ToString();
                num2 = 0;
            }

        }

        private decimal computeResult(string OP, decimal first, decimal second)
        {
            decimal result = 0;
            switch (OP)
            {
                case "+":
                    result = (first + second);
                    break;
                case "-":
                    result = (first - second);
                    break;
                case "×":
                    result = (first * second);
                    break;
                case "÷":
                    if (second != 0)
                        result = (first / second);
                    break;
            }
            return result;
        }                  
    }
}
