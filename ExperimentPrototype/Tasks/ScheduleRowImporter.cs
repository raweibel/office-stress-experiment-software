﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExperimentPrototype
{
    class ScheduleRowImporter
    {
        public ScheduleRow[] importScheduleRowList(string path)
        {
            List<ScheduleRow> a_list = new List<ScheduleRow>();

            string line;

            // Read the file and display it line by line. 

            System.IO.StreamReader file = new System.IO.StreamReader(path + @"ScheduleNames.txt", System.Text.Encoding.Default, false);
            List<String> readNames = new List<string>();
            while ((line = file.ReadLine()) != null)
            {
                readNames.Add(line);
            }
            file.Close();

            file = new System.IO.StreamReader(path + @"ScheduleEntries.txt", System.Text.Encoding.Default, false);
            List<String> readEntries = new List<string>();
            while ((line = file.ReadLine()) != null)
            {
                readEntries.Add(line);
            }
            file.Close();

            file = new System.IO.StreamReader(path + @"ScheduleAvailability.txt", System.Text.Encoding.Default, false);
            List<String> readAvailability = new List<string>();
            while ((line = file.ReadLine()) != null)
            {
                readAvailability.Add(line);
            }
            file.Close();

            for (int i = 0; i < readNames.Count*2; i = i+2) {
                
                ScheduleRow item = new ScheduleRow();

                item.Mitarbeiter = readNames[i / 2];

                string firstLine = readEntries[i];
                string secondLine = readEntries[i + 1];
                string[] splitFirst = firstLine.Split('\t');
                string[] splitSecond = secondLine.Split('\t');

                string[] splitAvailabilities1 = readAvailability[i].Split('\t');
                string[] splitAvailabilities2 = readAvailability[i+1].Split('\t');

                item.Montag_morgen.Entry = splitFirst[0];
                bool res = true;
                bool.TryParse(splitAvailabilities1[0], out res);
                item.Montag_morgen.Enabled = !res;
                item.Dienstag_morgen.Entry = splitFirst[1];
                res = true;
                bool.TryParse(splitAvailabilities1[1], out res);
                item.Dienstag_morgen.Enabled = !res;
                item.Mittwoch_morgen.Entry = splitFirst[2];
                res = true;
                bool.TryParse(splitAvailabilities1[2], out res);
                item.Mittwoch_morgen.Enabled = !res;
                item.Donnerstag_morgen.Entry = splitFirst[3];
                res = true;
                bool.TryParse(splitAvailabilities1[3], out res);
                item.Donnerstag_morgen.Enabled = !res;
                item.Freitag_morgen.Entry = splitFirst[4];
                res = true;
                bool.TryParse(splitAvailabilities1[4], out res);
                item.Freitag_morgen.Enabled = !res;
                item.Montag_nachmittag.Entry = splitSecond[0];
                res = true;
                bool.TryParse(splitAvailabilities2[0], out res);
                item.Montag_nachmittag.Enabled = !res;
                item.Dienstag_nachmittag.Entry = splitSecond[1];
                res = true;
                bool.TryParse(splitAvailabilities2[1], out res);
                item.Dienstag_nachmittag.Enabled = !res;
                item.Mittwoch_nachmittag.Entry = splitSecond[2];
                res = true;
                bool.TryParse(splitAvailabilities2[2], out res);
                item.Mittwoch_nachmittag.Enabled = !res;
                item.Donnerstag_nachmittag.Entry = splitSecond[3];
                res = true;
                bool.TryParse(splitAvailabilities2[3], out res);
                item.Donnerstag_nachmittag.Enabled = !res;                
                item.Freitag_nachmittag.Entry = splitSecond[4];
                res = true;
                bool.TryParse(splitAvailabilities2[4], out res);
                item.Freitag_nachmittag.Enabled = !res;

                a_list.Add(item);
            }

            return a_list.ToArray();
        }
    }

}
