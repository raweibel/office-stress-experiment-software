﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExperimentPrototype
{
    public class ChatMessage
    {
        public string message { get; set; }
        public int blockOrder { get; set; }
        public int blockNumber { get; set; }

        public int requiredEmail { get; set; }
    }
}
