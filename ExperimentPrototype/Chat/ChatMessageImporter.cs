﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ExperimentPrototype
{
    class ChatMessageImporter
    {
        public Queue<ChatMessage> importAllChatMessages(string path)
        {
            Queue<ChatMessage> messages = new Queue<ChatMessage>();

            string[] filepaths = Directory.GetFiles(path+ @"ChatMessages\", "chat*");

            foreach (string filepath in filepaths)
            {
                messages.Enqueue(getMessageFromText(filepath));
            }

            List<ChatMessage> sortedMessages = messages.ToList<ChatMessage>().OrderBy(o => o.blockNumber).ThenBy(c => c.blockOrder).ToList();

            return new Queue<ChatMessage>(sortedMessages); ;            
        }

        public ChatMessage getMessageFromText(string path)
        {
            ChatMessage a_message = new ChatMessage();

            System.Xml.Serialization.XmlSerializer reader = new System.Xml.Serialization.XmlSerializer(typeof(ChatMessage));
            System.IO.StreamReader file = new System.IO.StreamReader(
                path);
            a_message = (ChatMessage)reader.Deserialize(file);
            file.Close();

            return a_message;
        }
    }
}
