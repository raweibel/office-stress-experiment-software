﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExperimentPrototype
{
    class ChatMessageCollectionImporter
    {
        public ChatMessageCollection getMessagesFromText(string path)
        {
            ChatMessageCollection messageCollection = new ChatMessageCollection();

            System.Xml.Serialization.XmlSerializer reader = new System.Xml.Serialization.XmlSerializer(typeof(ChatMessageCollection));
            System.IO.StreamReader file = new System.IO.StreamReader(
                path);
            messageCollection = (ChatMessageCollection)reader.Deserialize(file);
            file.Close();

            return messageCollection;
        }
    }
}
