﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using System.Linq;
using System.Collections.Generic;

namespace ExperimentPrototype
{
    public partial class MainWindow
    {
        private void SendButtonClicked(object sender, RoutedEventArgs e)
        {
            SendChatMessage();
        }


        private void SendChatMessage()
        {
            TextBlock txBlock = new TextBlock();
            if (ChatInput.Text.Length > 0)
            {
                txBlock.Text = ChatInput.Text;
                ChatInput.Text = "";
                txBlock.HorizontalAlignment = HorizontalAlignment.Right;
                txBlock.TextWrapping = TextWrapping.Wrap;
                txBlock.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom(chatColorEmployee));
                txBlock.Margin = new Thickness(-1, -1, -1, -1);

                Style chatMessageParticipant_Style = FindResource("ChatMessageParticipant") as Style;
                Label messageLabel = new Label() { Style = chatMessageParticipant_Style };
                messageLabel.Width = Double.NaN;
                messageLabel.Height = Double.NaN;

                if (ChatMessageList.Items.Count > 0)
                {
                    Label previousItem = ((Label)ChatMessageList.Items[ChatMessageList.Items.Count - 1]);
                    TextBlock previousItemTextBlock = (TextBlock)previousItem.Content;
                    if (previousItemTextBlock.HorizontalAlignment == HorizontalAlignment.Left)
                        previousItemTextBlock.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom(chatColorBoss));
                }

                messageLabel.Content = txBlock;
                ChatMessageList.Items.Add(messageLabel);

                chatListToBottom();
                responsePending = false;

                if (logVerbosityLevel >= 1)
                    logger.LogMessage("Chat message send: " + txBlock.Text, 6);
            }
        }

        private void TriggerChatMessage(String message, bool animate)
        {
            ColorAnimation animation = new ColorAnimation();
            animation.From = Colors.Lime;
            animation.To = Colors.Green;
            animation.AutoReverse = true;
            animation.RepeatBehavior = RepeatBehavior.Forever;
            animation.Duration = new Duration(TimeSpan.FromSeconds(0.5));

            TextBlock txBlock = new TextBlock();
            txBlock.Text = message;
            txBlock.HorizontalAlignment = HorizontalAlignment.Left;
            txBlock.TextWrapping = TextWrapping.Wrap;
            txBlock.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom(chatColorBoss));
            txBlock.Margin = new Thickness(-1,-1,-1,-1);

            Style chatMessageBoss_Style = FindResource("ChatMessageBoss") as Style;
            Label messageLabel = new Label() { Style = chatMessageBoss_Style };
            messageLabel.Width = Double.NaN;
            messageLabel.Height = Double.NaN;           

            if (animate)
            {
                txBlock.Background.BeginAnimation(SolidColorBrush.ColorProperty, animation);               
            }
            if (ChatMessageList.Items.Count > 0)
            {
                Label previousItem = ((Label)ChatMessageList.Items[ChatMessageList.Items.Count - 1]);
                TextBlock previousTextBlock = (TextBlock)previousItem.Content;
                if (previousTextBlock.HorizontalAlignment == HorizontalAlignment.Left)
                    previousTextBlock.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom(chatColorBoss));
            }

            /*
            txBlock.Width = Double.NaN;
            txBlock.Height = Double.NaN;
            txBlock.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom(chatColorBoss));
            txBlock.Foreground = Brushes.White;
            txBlock.FontSize = 15;
            
            if (animate)
                txBlock.Background.BeginAnimation(SolidColorBrush.ColorProperty, animation);
            if (ChatMessageList.Items.Count > 0)
            {
                TextBlock previousItem = ((TextBlock)ChatMessageList.Items[ChatMessageList.Items.Count - 1]);                
                if (previousItem.HorizontalAlignment == HorizontalAlignment.Left)
                    previousItem.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom(chatColorBoss));
            }
             */
            messageLabel.Content = txBlock;
            ChatMessageList.Items.Add(messageLabel);

            chatListToBottom();
        }

        private void EnterPressedInTextBox(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return && !e.IsRepeat)
            {
                SendChatMessage();
            }
        }

        private void chatListToBottom()
        {
            if (VisualTreeHelper.GetChildrenCount(ChatMessageList) > 0)
            {
                Border border = (Border)VisualTreeHelper.GetChild(ChatMessageList, 0);
                ScrollViewer scrollViewer = (ScrollViewer)VisualTreeHelper.GetChild(border, 0);
                scrollViewer.ScrollToBottom();
            }
        }

        private void StartChatTimer()
        {
            chatMessages = new Queue<ChatMessage>(chatMessages.Where(m => m.blockNumber > currentBlock).ToList());
            chatTimer = new System.Windows.Threading.DispatcherTimer();
            chatTimer.Tick += new EventHandler(ChatTrigger_tick);
            chatTimer.Interval = chatInterval;
            chatTimer.Start();
        }

        private int r = 0;

        private void ChatTrigger_tick(object sender, EventArgs e)
        {
            if (chatMessages.Count > 0)
            {
                // only write new message if no response pending
                if (!responsePending)
                {
                    // Send next message
                    ChatMessage nextMessage = chatMessages.Peek();

                    if (nextMessage.blockNumber - 1 == currentBlock && nextMessage.requiredEmail <= currentBlockOrder)
                    {
                        // if not first message responde to previous message
                        if (!firstChatMessage && !hrmessage)
                        {                           
                            string[] chatReplies = { "Danke", "Alles klar", "Okay", "Merci", "Ok" };
                            r = (r + 1) % 5;
                            TriggerChatMessage(chatReplies[r], true);
                        }

                        hrmessage = false;

                        // Wait a short moment before sending next message
                        delayedResponse = new System.Windows.Threading.DispatcherTimer();
                        delayedResponse.Tick += new EventHandler(DelayedChatMessage);
                        delayedResponse.Interval = responseDelay;
                        delayedResponse.Start();
                    }
                }
            }
        }

        private void DelayedChatMessage(object sender, EventArgs e)
        {
            if (chatMessages.Count > 0)
            {
                ChatMessage nextMessage = chatMessages.Dequeue();
                TriggerChatMessage(nextMessage.message, true);

                if (logVerbosityLevel >= 2)
                    logger.LogMessage("Chat message received: Block:" + nextMessage.blockNumber + " Order" + nextMessage.blockOrder, 6);

                responsePending = true;
                if (!firstChatMessage)
                {
                    Label previousItem = ((Label)ChatMessageList.Items[ChatMessageList.Items.Count - 2]);
                    TextBlock previousTextBlock = null;
                    if (previousItem != null)
                        previousTextBlock = (TextBlock)previousItem.Content;
                    if (previousTextBlock.HorizontalAlignment == HorizontalAlignment.Left)
                        previousTextBlock.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom(chatColorBoss));
                }
                firstChatMessage = false;
            }
           
            delayedResponse.Stop();
        }

        public void setupChatforGroup()
        {
            // Wait a short moment before sending next message
            delayedGroupMessageTimer = new System.Windows.Threading.DispatcherTimer();
            delayedGroupMessageTimer.Tick += new EventHandler(setupChatforGroupDelayed);
            delayedGroupMessageTimer.Interval = new TimeSpan(0, 0, 2);
            delayedGroupMessageTimer.Start();

            extraChatCounter = 0;

            switch (experimentalGroup)
            {
                case 0:

                case 1:                  
                    ChatPartnerStatus.Content = "Nicht verfügbar";
                    break;
                case 2:
                    ChatMessageImporter chatImport = new ChatMessageImporter();
                    if (experimentalGroup == 2)
                        chatMessages = chatImport.importAllChatMessages(resourcesPath);
                    ChatPartnerStatus.Content = "In einer Besprechung";
                    break;
            }
        }

        private void setupChatforGroupDelayed(object sender, EventArgs e)
        {            
            // Import chat messages for later use.
            ChatMessageImporter chatImport = new ChatMessageImporter();

            switch (extraChatCounter)
            {
                case 0:
                    ChatNotification.Visibility = Visibility.Visible;
                    ChatPartnerStatus.Content = "Online";
                    ChatPartnerStatus.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF0BB101"));
                    delayedGroupMessageTimer.Interval = new TimeSpan(0, 0, 5);
                    break;
                case 1:

                case 2:

                case 3:
                    delayedGroupMessageTimer.Interval = new TimeSpan(0, 0, 10);
                    switch (experimentalGroup)
                    {
                        case 0:

                        case 1:
                            //Load initial message from xml!
                            ChatMessage initialMessage = chatImport.getMessageFromText(resourcesPath + @"\ChatMessages\SpecialMessages\NoInterruptions" + extraChatCounter + ".xml");
                            TriggerChatMessage(initialMessage.message, true);
                            break;
                        case 2:
                            initialMessage = chatImport.getMessageFromText(resourcesPath + @"\ChatMessages\SpecialMessages\WithInterruptions" + extraChatCounter + ".xml");
                            TriggerChatMessage(initialMessage.message, true);
                            break;
                    }
                    break;
                case 4:                    
                    delayedGroupMessageTimer.Interval = new TimeSpan(0, 0, 30);
                    break;
                case 5:
                    ChatPartnerStatus.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FFFF0404"));

                    switch (experimentalGroup)
                    {
                        case 0:

                        case 1:
                            ChatPartnerStatus.Content = "Nicht verfügbar";
                            break;
                        case 2:
                            ChatPartnerStatus.Content = "In einer Besprechung";
                            break;
                    }
                    delayedGroupMessageTimer.Stop();
                    break;
            }
            extraChatCounter++;
        }


        public void sendHRMessage()
        {
            // Wait a short moment before sending next message
            delayedHRmessageTimer = new System.Windows.Threading.DispatcherTimer();
            delayedHRmessageTimer.Tick += new EventHandler(sendHRMessageDelayed);
            delayedHRmessageTimer.Interval = new TimeSpan(0,0,10);
            delayedHRmessageTimer.Start();
            extraChatCounter = 0;
        }

        public void sendHRMessageDelayed(object sender, EventArgs e) {
           
            ChatMessageImporter chatImport = new ChatMessageImporter();
            if (ChatMessageList.Items.Count > 1)
            {
                Label previousItem = ((Label)ChatMessageList.Items[ChatMessageList.Items.Count - 1]);
                TextBlock previousTextBlock = null;
                if (previousItem != null)
                    previousTextBlock = (TextBlock)previousItem.Content;
                    if (previousTextBlock.HorizontalAlignment == HorizontalAlignment.Left)
                        previousTextBlock.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom(chatColorBoss));
            }

            switch (extraChatCounter)
            {
                case 0:
                    ChatNotification.Visibility = Visibility.Visible;
                    ChatPartnerStatus.Content = "Online";
                    ChatPartnerStatus.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF0BB101"));
                    break;
                case 1:

                case 2:

                case 3:
                    delayedHRmessageTimer.Interval = new TimeSpan(0, 0, 10);
                    switch (experimentalGroup)
                    {
                        case 0:
                            //Load initial message from xml!
                            ChatMessage HRMessage = chatImport.getMessageFromText(resourcesPath + @"\ChatMessages\SpecialMessages\Control" + extraChatCounter + ".xml");
                            TriggerChatMessage(HRMessage.message, true);
                            break;
                        case 1:

                        case 2:
                            HRMessage = chatImport.getMessageFromText(resourcesPath + @"\ChatMessages\SpecialMessages\Stress" + extraChatCounter + ".xml");
                            TriggerChatMessage(HRMessage.message, true);
                            break;
                    }
                    break;
                case 4:
                    delayedHRmessageTimer.Interval = new TimeSpan(0, 0, 30);
                    break;
                case 5:
                    ChatPartnerStatus.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FFFF0404"));

                    switch (experimentalGroup)
                    {
                        case 0:

                        case 1:
                            ChatPartnerStatus.Content = "Nicht verfügbar";
                            break;
                        case 2:
                            ChatPartnerStatus.Content = "In einer Besprechung";
                            break;
                    }
                    delayedHRmessageTimer.Stop();
                    break;
            }
            extraChatCounter++;                     
                
            hrmessage = true;
            responsePending = false;
        }

        private void NotificationButton_Click(object sender, RoutedEventArgs e)
        {
            ChatNotification.Visibility = Visibility.Hidden;
        }
    }    
}