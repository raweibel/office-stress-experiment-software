﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace ExperimentPrototype
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        void App_Startup(object sender, StartupEventArgs e)
        {
            
            EventManager.RegisterClassHandler(typeof(DatePicker),
                DatePicker.LoadedEvent,
                new RoutedEventHandler(DatePicker_Loaded));
            // Application is running
            // Process command line args
            string a_participantId = "";

            // 0: Control
            // 1: Stress 1
            // 2: Stress 2 (highest; with interuptions)
            int an_experimentalGroup = 0;
            int blockNumber = 0, syncNumber = -1;
            for (int i = 0; i != e.Args.Length; ++i)
            {
                if (e.Args[i] == "-p")
                {

                    a_participantId = e.Args[i + 1];

                }

                if (e.Args[i] == "-c")
                {
                    int.TryParse(e.Args[i + 1], out an_experimentalGroup);

                }

                if (e.Args[i] == "-b")
                {
                    int.TryParse(e.Args[i + 1], out blockNumber);

                }

                if (e.Args[i] == "-s")
                {
                    int.TryParse(e.Args[i + 1], out syncNumber);

                }
            }

            // Create main application window
            MainWindow mainWindow;
            if (a_participantId.Length > 0)
            {
                mainWindow = new MainWindow(a_participantId, an_experimentalGroup, blockNumber ,syncNumber);                
            } else
            {
                mainWindow = new MainWindow("NoIdGiven", -1,0, 0);
            }
            mainWindow.Show();
        }

        public static T GetChildOfType<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj == null) return null;

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
            {
                var child = VisualTreeHelper.GetChild(depObj, i);

                var result = (child as T) ?? GetChildOfType<T>(child);
                if (result != null) return result;
            }
            return null;
        }

        void DatePicker_Loaded(object sender, RoutedEventArgs e)
        {
            var dp = sender as DatePicker;
            if (dp == null) return;

            var tb = GetChildOfType<DatePickerTextBox>(dp);
            if (tb == null) return;

            var wm = tb.Template.FindName("PART_Watermark", tb) as ContentControl;
            if (wm == null) return;

            wm.Content = "Datum auswählen";
        }
    }
}
