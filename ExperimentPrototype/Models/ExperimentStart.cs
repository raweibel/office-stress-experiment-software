﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExperimentPrototype.Models
{
    public class ExperimentStart
    {
        public int ExperimentStartId { get; set; }
        public bool startExperiment { get; set; }
    }
}
