﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExperimentPrototype.Models
{
    public class LogEvent
    {
        public int LogEventId{ get; set; }
        public string ParticipantId { get; set; }
        public string LogMessage { get; set; }
        public int Category { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
