﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExperimentPrototype.Models
{
    public class CaseReportContext : DbContext
    {
        public DbSet<CaseReport> CaseReports { get; set; }

        public CaseReportContext()
             : base("name=dbConnection")
        {

        }

    }
}
