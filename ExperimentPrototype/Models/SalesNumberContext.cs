﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExperimentPrototype.Models
{
    public class SalesNumberContext : DbContext
    {
        public DbSet<SalesNumber> SalesNumbers { get; set; }

        public SalesNumberContext()
             : base("name=dbConnection")
        {

        }
    }
}
