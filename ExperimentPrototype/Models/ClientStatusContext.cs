﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExperimentPrototype.Models
{
    public class ClientStatusContext : DbContext
    {
        public DbSet<ClientStatus> ClientStatuses { get; set; }

        public ClientStatusContext()
             : base("name=dbConnection")
        {

        }
    }
}
