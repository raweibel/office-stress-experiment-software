﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExperimentPrototype.Models
{
    public class LogContext : DbContext
    {
        public DbSet<LogEvent> LogEvents { get; set; }

        public LogContext()
             : base("name=dbConnection")
        {

        }
        
    }
}
