﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExperimentPrototype.Models
{
    public class ExperimentStartContext : DbContext
    {
        public DbSet<ExperimentStart> ExperimentStarts { get; set; }

        public ExperimentStartContext()
             : base("name=dbConnection")
        {

        }
    }
}
