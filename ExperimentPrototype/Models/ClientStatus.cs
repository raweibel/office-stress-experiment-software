﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExperimentPrototype.Models
{

    public class ClientStatus
    {
        public int ClientStatusId { get; set; }
        public string ParticipantId { get; set; }
        public bool IsReady { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
