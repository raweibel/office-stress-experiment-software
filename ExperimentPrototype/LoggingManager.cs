﻿using ExperimentPrototype.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExperimentPrototype
{
    class LoggingManager
    {
        private string participantId;

        public LoggingManager(string a_participantId)
        {
            participantId = a_participantId;
        }


        public void LogMessage(string message, int category)
        {
            using (var context = new LogContext())
            {
                context.Database.Connection.Open();
                var le = new LogEvent { ParticipantId = participantId, LogMessage = message, Category = category, Timestamp = DateTime.Now };
                context.LogEvents.Add(le);
                context.SaveChanges();
               
            }
        }

        public void LogSpreadsheet(SalesNumber[] sales, int blockId)
        {
            // Loop through all items and store them with a timestamp
            using (var context = new SalesNumberContext())
            {
                context.Database.Connection.Open();
                sales.Select(c => { c.participantId = participantId; return c; }).ToList();
                sales.Select(c => { c.blockId = blockId; return c; }).ToList();
                context.SalesNumbers.AddRange(sales);
                context.SaveChanges();
            }
        }

        public void LogSchedule(ScheduleRow[] rows)
        {
            // Loop through all items and store them with a timestamp
        }
    }
}
