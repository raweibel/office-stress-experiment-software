﻿using System;
using System.Windows;
using System.Windows.Media;

namespace ExperimentPrototype
{
    public partial class MainWindow
    {

        private void StartsyncDispatchTimer()
        {
            Synchronization_Text.Text = "Bitte warten Sie kurz. Das Experiment geht gleich weiter.";
            syncDispatchTimer = new System.Windows.Threading.DispatcherTimer();
            syncDispatchTimer.Tick += new EventHandler(syncDispatchTimer_tick);
            syncDispatchTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
            syncDispatchTimer.Start();

            if (syncCounter == -1)
            {
                setReadyInDB();
            }
        }

        private void syncDispatchTimer_tick(object sender, EventArgs e)
        {

            if (syncCounter == -1)
            {
                if (IsReleasedInDB())
                {
                    syncCounter++;
                    syncDispatchTimer.Stop();
                    loadNextInstruction();
                    InstructionScreen.Visibility = Visibility.Visible;
                    SynchronizationScreen.Visibility = Visibility.Hidden;
                    experimentZeroPoint = DateTime.Now;

                    if (logVerbosityLevel >= 1)
                        logger.LogMessage("Timer started", 1);
                }
            }
            else
            {
                if ((DateTime.Now - experimentZeroPoint).TotalMinutes > timings[syncCounter])
                {
                    syncDispatchTimer.Stop();
                    syncCounter++;
                    switch (currentBlock)
                    {
                        case 0:

                            Synchronization_Text.Text = "Liebe/r Teilnehmer/in, bitte beginnen Sie nun mit der Büroaufgabe.";
                            break;
                        case 3:
                            Synchronization_Text.Text = "Bitte bleiben Sie an Ihrem Arbeitsplatz sitzen.";
                            break;
                        default:
                            Synchronization_Text.Text = "Liebe/r Teilnehmer/in, bitte fahren Sie nun mit der Büroaufgabe fort.";
                            break;
                    }
                    delayedBlockStartTimer = new System.Windows.Threading.DispatcherTimer();
                    delayedBlockStartTimer.Tick += new EventHandler(delayedBlockStart);
                    delayedBlockStartTimer.Interval = new TimeSpan(0, 0, 0, 5);
                    delayedBlockStartTimer.Start();
                }
            }
        }

        private void delayedBlockStart(object sender, EventArgs e)
        {
            TimeSpan elapsedTime = DateTime.Now - experimentZeroPoint;
            TimeSpan diff = TimeSpan.FromMinutes(timings[syncCounter]) - elapsedTime;
            syncCounter++;
            StartBlockTimer(diff);
            if (logVerbosityLevel >= 1)
                logger.LogMessage("Block " + (currentBlock + 1) + " start", 1);
            SynchronizationScreen.Visibility = Visibility.Hidden;
            if (currentBlock == 0)
                setupChatforGroup();
            if (currentBlock == stressBlock - 1)
            {
                // Show message for stress block
                StressBlockScreen.Visibility = Visibility.Visible;
                delayedBlockStartTimer.Stop();
                ChatPartnerStatus.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FFFF0404"));
                ChatPartnerStatus.Content = "Offline";
            }
            else
            {
                if (currentBlock == HRAnnouncementBlock - 1)
                {
                    sendHRMessage();
                    StartShortEmailTimer();
                    DelayedStartHRAnnouncement();
                }
                else
                {
                    StartEmailTimer();
                    if (experimentalGroup == 2)
                        StartChatTimer();
                }
                delayedBlockStartTimer.Stop();

            }
        }

        private void StartBlockTimer(TimeSpan time)
        {
            blockTimer = new System.Windows.Threading.DispatcherTimer();
            blockTimer.Tick += new EventHandler(BlockFinished_tick);
            if (time.TotalSeconds > 0)
                blockTimer.Interval = time;
            else
                blockTimer.Interval = new TimeSpan(0, 0, 1);
            blockTimer.Start();

        }

        private void BlockFinished_tick(object sender, EventArgs e)
        {
            blockTimer.Stop();
            emailTimer.Stop();
            if (chatTimer != null)
                chatTimer.Stop();
            if (logVerbosityLevel >= 1)
                logger.LogMessage("Block " + (currentBlock + 1) + " end", 1);
            /*if (logVerbosityLevel >= 2)
            {
                List<SalesNumber> numbers = new List<SalesNumber>();
                foreach (var item in Spreadsheet.Items)
                {
                    numbers.Add(item as SalesNumber);
                }
                logger.LogSpreadsheet(numbers.ToArray(), currentBlock);
            }*/
            currentBlock++;
            InstructionScreen.Visibility = Visibility.Visible;
            StressBlockScreen.Visibility = Visibility.Hidden;

            loadNextInstruction();
        }

        private void DelayedStartHRAnnouncement()
        {
            HRannouncementStartTimer = new System.Windows.Threading.DispatcherTimer();
            HRannouncementStartTimer.Tick += new EventHandler(DelayedStartHRAnnouncement_tick);
            HRannouncementStartTimer.Interval = HRAnnouncmentDelay;
            HRannouncementStartTimer.Start();
        }

        private void DelayedStartHRAnnouncement_tick(object sender, EventArgs e)
        {
            HRannouncementStartTimer.Stop();
            StressBlockScreen.Visibility = Visibility.Visible;
            StartHRAnnouncement();
        }

        private void StartHRAnnouncement()
        {
            HRannouncementEndTimer = new System.Windows.Threading.DispatcherTimer();
            HRannouncementEndTimer.Tick += new EventHandler(StartHRAnnouncement_tick);
            HRannouncementEndTimer.Interval = HRAnnouncmentDuration;
            HRannouncementEndTimer.Start();
        }

        private void StartHRAnnouncement_tick(object sender, EventArgs e)
        {
            HRannouncementEndTimer.Stop();
            StressBlockScreen.Visibility = Visibility.Hidden;
            StartEmailTimer();
            if (experimentalGroup == 2)
                StartChatTimer();
        }
    }
}
