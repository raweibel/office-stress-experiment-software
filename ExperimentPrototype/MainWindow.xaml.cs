﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.IO;
using System.Collections.ObjectModel;
using System.Linq;
using ExperimentPrototype.Models;
using System.Windows.Xps.Packaging;
using System.Windows.Media.Imaging;

namespace ExperimentPrototype
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public string participantId { get; set; }
        public int experimentalGroup { get; set; }

        // ##### Paramters to be changed
        private string resourcesPath = @"..\..\Resources\";
        private TimeSpan emailInterval = new TimeSpan(0, 0, 30), chatInterval = new TimeSpan(0, 2, 0), responseDelay = new TimeSpan(0,0,5), HRAnnouncmentDelay = new TimeSpan(0, 1, 55), HRAnnouncmentDuration = new TimeSpan(0, 2, 0);
        private double[] timings; // In minutes
        private int HRAnnouncementBlock = 3, stressBlock = 4;
        public string chatColorBoss = "#FF5BD66A", chatColorEmployee = "#FF5292BC";
        private int logVerbosityLevel = 10;
        private TimeSpan maxWaitEmail = new TimeSpan(0, 2, 30);
        // #####

        private ObservableCollection<SimulatedEmail> displayedMails = new ObservableCollection<SimulatedEmail>();
        private Queue<SimulatedEmail> mails;
        private Queue<Instruction> instructions;
        private Queue<ChatMessage> chatMessages;

        // from currentBlock to actual block always add 1 (starts with 0)!
        private int currentBlock, syncCounter = -1;
        private int currentBlockOrder, extraChatCounter;
        private DateTime experimentZeroPoint, QuestionnaireStart;
        private System.Windows.Threading.DispatcherTimer blockTimer, syncDispatchTimer, emailTimer, chatTimer, delayedResponse, questionnaireTimer, startQuestionnaireTimer, instructionSwitchTimer;  

        private System.Windows.Threading.DispatcherTimer delayedBlockStartTimer, delayedHRmessageTimer, delayedGroupMessageTimer, specialMailTimer, HRannouncementStartTimer, HRannouncementEndTimer;

        private SimulatedEmail selectedEmail;
        private bool responsePending, firstChatMessage = true, hrmessage;
        private LoggingManager logger;

        public MainWindow(string participantId, int groupdCondition, int a_blockNumber, int syncNumber)
        {
            this.participantId = participantId;
            experimentalGroup = groupdCondition;
            currentBlock = a_blockNumber;            
            logger = new LoggingManager(participantId);

            System.IO.StreamReader file = new System.IO.StreamReader(resourcesPath + @"BlockTimes.txt", System.Text.Encoding.Default,false);
            string line = file.ReadLine();
            string[] splitLine = line.Split(',');
            timings = Array.ConvertAll(splitLine, double.Parse);
            file.Close();

            if (a_blockNumber > 0)
            {                
                syncCounter = syncNumber;                
                experimentZeroPoint = DateTime.Now -  TimeSpan.FromMinutes(timings[syncNumber-1]) ;
            }
           
            InitializeComponent();

            BitmapImage bitmap = new BitmapImage();
            bitmap.BeginInit();       
            bitmap.UriSource = new Uri(AppDomain.CurrentDomain.BaseDirectory + resourcesPath + @"Insurance_Logo.jpg");
            bitmap.EndInit();
            Insurance_Logo.Source = bitmap;

            ObservableCollection<SalesNumber> sales;
            SalesNumberNamesImporter salesImport = new SalesNumberNamesImporter();
            sales = new ObservableCollection <SalesNumber>(salesImport.importAgentNames(resourcesPath));
            Spreadsheet.ItemsSource = sales;
            //Spreadsheet.DataContext = sales;

            // Import the first email to be displayed from the beginning
            EmailImport emailImport = new EmailImport();
            mails = emailImport.importAllEmails(resourcesPath);
            SimulatedEmail nextMail = mails.Dequeue();
            nextMail.received = DateTime.Now;
            displayedMails.Add(nextMail);
            nextMail = mails.Dequeue();
            nextMail.received = DateTime.Now;
            displayedMails.Add(nextMail);
            nextMail = mails.Dequeue();
            nextMail.received = DateTime.Now;
            displayedMails.Add(nextMail);
            EmailOverview.ItemsSource = displayedMails;
            ReadWindow.Visibility = Visibility.Hidden;

            string fileName = resourcesPath + "Debriefing.xps" ;
            XpsDocument doc = new XpsDocument(fileName, FileAccess.Read);
            Debriefing.Document = doc.GetFixedDocumentSequence();

            // Hide screens for later use
            SynchronizationScreen.Visibility = Visibility.Hidden;            
            EndScreen.Visibility = Visibility.Hidden;
            Questionnaire.Visibility = Visibility.Hidden;
            StressBlockScreen.Visibility = Visibility.Hidden;
            ChatNotification.Visibility = Visibility.Hidden;

            //Import instructions from textfiles
            InstructionImporter instructImport = new InstructionImporter();
            instructions = instructImport.importAllInstructions(resourcesPath);
            if (syncNumber < 1)
            {
                Instruction firstInstruction = instructions.Dequeue();
                Instruction_Text.Text = firstInstruction.text;
            }

            // Import client list for schedule task
            ClientListImporter clientListImport = new ClientListImporter();
            ClientListItem[] clientList = clientListImport.importClientList(resourcesPath);
            ClientList.ItemsSource = clientList;

            // Import schedule entries for schedule task
            ScheduleRowImporter scheduleRowImport = new ScheduleRowImporter();
            ScheduleRow[] scheduleRows = scheduleRowImport.importScheduleRowList(resourcesPath);
            CalListBox.ItemsSource = scheduleRows.ToList().GetRange(0,12);
            CalListBox_Copy.ItemsSource = scheduleRows.ToList().GetRange(12, 16);
            
            //currentBlock = 0;            

            // Initialize the buttons
            SendButton.Click += SendButtonClicked;
            ChatInput.KeyUp += EnterPressedInTextBox;

            // Setup clock
            System.Windows.Threading.DispatcherTimer timer = new System.Windows.Threading.DispatcherTimer(new TimeSpan(0, 0, 1), System.Windows.Threading.DispatcherPriority.Normal, delegate
            {
                this.Clock.Content = DateTime.Now.ToString("d/M/yyyy HH:mm:ss");
            }, this.Dispatcher);

            if (syncNumber > 0)
            {
                instructions = new Queue<Instruction>(instructions.Where(m => m.block_id >= currentBlock).ToList());
                loadNextInstruction();
                setupChatforGroup();
            }                

            // Reset status in DB
            ResetStatusInDB();
        }                 


        private void ResetStatusInDB()
        {           
            using (var cSStatus = new ClientStatusContext())
            {
                cSStatus.Database.Connection.Open();
                var existingEntry = cSStatus.ClientStatuses.Where(client => client.ParticipantId == participantId).FirstOrDefault();
                if(existingEntry == null)
                {
                    var cs = new ClientStatus { ParticipantId = participantId, IsReady = false, Timestamp = DateTime.Now};
                    cSStatus.ClientStatuses.Add(cs);
                    cSStatus.SaveChanges();
                } else
                {
                    existingEntry.IsReady = false;
                    existingEntry.Timestamp = DateTime.Now;
                    cSStatus.SaveChanges();
                }
               
            }
        }

        private bool IsReleasedInDB()
        {
            using (var expStartContext = new ExperimentStartContext())
            {
                expStartContext.Database.Connection.Open();
                var startEntry = expStartContext.ExperimentStarts.Find(1);

                if (startEntry != null)
                {
                    if (startEntry.startExperiment)
                    {
                        return true;
                    } else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }

        }

        private void setReadyInDB()
        {
            using (var cSStatus = new ClientStatusContext())
            {
                cSStatus.Database.Connection.Open();
                var existingEntry = cSStatus.ClientStatuses.Where(client => client.ParticipantId == participantId).FirstOrDefault();
                if (existingEntry == null)
                {
                    var cs = new ClientStatus { ParticipantId = participantId, IsReady = true, Timestamp = DateTime.Now };
                    cSStatus.ClientStatuses.Add(cs);
                    cSStatus.SaveChanges();
                }
                else
                {
                    existingEntry.IsReady = true;
                    existingEntry.Timestamp = DateTime.Now;
                    cSStatus.SaveChanges();
                }

            }
        }

        void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.Source is TabControl)
            {
                var tab = sender as TabControl;
                var item = tab.SelectedItem as TabItem;

                if (item != null)
                {
                    if (logVerbosityLevel >= 2)
                        logger.LogMessage("Switched to tab: " + item.Name, 2);
                }
                
            }
        }

    }
}
